package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Task(val id: String, var title: String, var date: String, var checked: Boolean)

