package com.example.model

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.io.path.writeText

/**
 * Hace todas las operaciones inherentes a las Task,
 * crea o lee ficheros, etc.
 */

class TasksRepository() {
    private val path = Path("ficheros/todoitems.json")
    private var listTasks : MutableList<Task>
    init {
        listTasks = readListTaskFromFile(path)
    }

    fun getListTasks() : List<Task> = listTasks

    /**
     * Fallo al escribir la lista (correctamente modificada)
     * en el fichero json:
     * Agrega la tarea borrada fuera de la lista,
     * ocasionando un error al volver a iniciar
     * el servidor, por no obtener una lista de
     * tareas.
     */
    fun deleteTask(id : String): MutableList<Task> {
        for (i in 0..listTasks.lastIndex){
            if (listTasks[i].id == id){
                listTasks.removeAt(i)
                break
            }
        }
        val listTasksEncoded = listToJsonData(listTasks)
        writeInPath(path,listTasksEncoded)
        return listTasks

    }
    fun modifyTask(id : String, newTask: Task): MutableList<Task>{
        for (i in 0..listTasks.lastIndex){
            if (listTasks[i].id == id){
                listTasks[i]=newTask
                break
            }
        }
        val listTasksEncoded = listToJsonData(listTasks)
        writeInPath(path, listTasksEncoded)
        return listTasks
    }

    fun newTaskAdd(task: Task) {
        listTasks += task
        val listTaskEncoded = listToJsonData(listTasks)
        writeInPath(path, listTaskEncoded)
    }

    private fun listToJsonData(listTasks: MutableList<Task>): String {
        val serializer = Json { ignoreUnknownKeys = true }
        return serializer.encodeToString<List<Task>>(listTasks)
    }

    private fun readListTaskFromFile(path: Path): MutableList<Task> {
        val text = path.readText()
        val serializer = Json { ignoreUnknownKeys = true }
        return serializer.decodeFromString<MutableList<Task>>(text)
    }

    fun checkPath(path: Path) = path.toFile().exists()

    fun createPath(path: Path, jsonData: String) {
        path.writeText("", options = arrayOf(StandardOpenOption.CREATE))
    }

    fun writeInPath(path: Path, jsonData: String) {
        path.writeText(jsonData, options = arrayOf(StandardOpenOption.WRITE))
    }

    fun appendInPath(path: Path, jsonData: String) { //left verify
        path.writeText(jsonData, options = arrayOf(StandardOpenOption.APPEND))
    }

}