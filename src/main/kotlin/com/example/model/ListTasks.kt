package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class ListTasks(val id: String, val name: String, val tasks: MutableList<Task>)