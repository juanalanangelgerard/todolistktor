package com.example

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*

fun main() {
//    embeddedServer(Netty, port = 8080)
        embeddedServer(Netty, port = System.getenv("PORT").toInt()){
        install(CORS){
            anyHost()
            allowHeader(HttpHeaders.ContentType)
            allowHeader(HttpHeaders.Authorization)
            allowMethod(HttpMethod.Options)
            allowMethod(HttpMethod.Put)
            allowMethod(HttpMethod.Patch)
            allowMethod(HttpMethod.Delete)
//            allowHost("0.0.0.0:8081")
//            allowHeader(HttpHeaders.ContentType)
        }
        configureSerialization()
        configureRouting()
    }.start(wait = true)
}

