package com.example.routes


import com.example.model.TasksRepository
import com.example.model.*

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*


/**
 *
 * GET http://localhost:8080/todoitems
Accept: application/json


###

POST http://localhost:8080/todoitems
Content-Type: application/json

{
"id": "1",
"title": "tit1",
"date": "dat1",
"hour": "hour1",
"checked": false
}
 *
 */

fun Route.taskRouting(){
    val tasksRepository: TasksRepository = TasksRepository()
    route("/") {

        get("todoitems"){
            call.respond(tasksRepository.getListTasks())
        }

        get("todoitems/lastID"){
            call.respond(tasksRepository.getListTasks().size)
        }

        get("todoitems/{id?}"){

            val id = call.parameters["id"] ?: return@get call.respondText(
                "Falta id.",
                status = HttpStatusCode.BadRequest
            )
            val task = tasksRepository.getListTasks().find { it.id == id } ?: return@get call.respondText(
                "No existe tarea con el id ($id) ingresado.",
                status = HttpStatusCode.NotFound
            )
            call.respond(task)
        }

        post("todoitems/add") {
            val task = call.receive<Task>()
            tasksRepository.newTaskAdd(task)
            call.respondText("Tarea agregada correctamente:\n   $task", status = HttpStatusCode.Created)
        }

        put("update/{id?}") {
            val id = call.parameters["id"]
            val task = call.receive<Task>()
            tasksRepository.modifyTask(id!!, task)
            call.respondText("Tarea editada correctamente", status = HttpStatusCode.OK)
        }


        delete("delete/{id?}") {
            val id = call.parameters["id"]
            val resultDeleted = tasksRepository.deleteTask(id!!)
            call.respondText("$resultDeleted", status = HttpStatusCode.Gone)

        }


    }

}
/*
fun Route.customerRouting() {
    route("/film") {
        get("all") {
            if (filmStorage.isNotEmpty()) {
                call.respond(filmStorage)
            } else {
                call.respondText("No movie found", status = HttpStatusCode.OK)
            }
        }
        get("{id?}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val film =
                filmStorage.find { it.id == id } ?: return@get call.respondText(
                    "No movie with id $id",
                    status = HttpStatusCode.NotFound
                )
            call.respond(film)
        }
        post ("add"){
            val movie = call.receive<Film>()
            filmStorage.add(movie)
            call.respondText("Movie stored correctly", status = HttpStatusCode.Created)

            var fileName = ""
            var id = ""
            var title = ""
            var year = ""
            var genre = ""
            var director = ""
            var image = ""
            val guyList = mutableListOf<Film>()

            val datos = call.receiveMultipart()
            datos.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        if(part.name == "id"){
                            id = part.value
                        }
                        if (part.name == "title"){
                            title = part.value
                        }
                        if (part.name == "year"){
                            year = part.value
                        }
                        if (part.name == "genre"){
                            genre = part.value
                        }
                        if (part.name == "director"){
                            director = part.value
                        }
                        if (part.name == "image"){
                            image = part.value
                        }
                    }
                    is PartData.FileItem -> {
                        fileName = part.originalFileName as String
                        var fileBytes = part.streamProvider().readBytes()
                        File("uploads/$fileName").writeBytes(fileBytes)
                    }
                    else -> {}
                }
            }
            val film = Film(id, title, year, genre, director, image )
            guyList.add(film)
            call.respondText("Film stored correctly and \"$fileName is uploaded to 'uploads/$fileName'\"", status = HttpStatusCode.Created)



        }
        post("{id?}/add") {
            val id = call.parameters["id"] ?: return@post call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val comment = call.receive<Comment>()
            commentStorage.add(comment)
            call.respondText("Comment stored correctly", status = HttpStatusCode.Created)

        }
        put("update/{id?}") {
            val id = call.parameters["id"] ?: return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val film = call.receive<Film>()
            for (i in 0..filmStorage.lastIndex) {
                if (filmStorage[i].id == id) {
                    filmStorage[i] = film
                    call.respondText("Film updated correctly", status = HttpStatusCode.Accepted)
                }
            }
        }

        delete("delete/{id?}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (filmStorage.removeIf { it.id == id }) {
                call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not Found", status = HttpStatusCode.NotFound)
            }
        }
        get ("{id}/comments") {
            val id = call.parameters["id"]
            val comments = commentStorage.filter { it.filmId == id }
            if (comments.isNotEmpty()) {
                call.respond(comments)
            }else{
                call.respondText("No customers found", status = HttpStatusCode.OK)
            }
        }
    }
}*/
